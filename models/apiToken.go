package models

//a struct to rep user account
type ApiKey struct {
	ID       uint   `gorm:"primary_key"`
	UserId   uint   `json:"user_id"`
	DomainId uint   `json:"domain_id"`
	IsTest         bool      `json:"is_test"`
}