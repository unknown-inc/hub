package models

import "time"

//a struct to rep user account
type SubscriptionPlanUsage struct {
	ID       uint `json:"id"`
	From      time.Time `json:"from"`
	To      time.Time `json:"to"`
	PlanId      time.Time `json:"plan_id"`
}
