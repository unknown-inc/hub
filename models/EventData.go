package models

import "time"

type EventType string

const (
	CLICK       EventType = "CLICK"
	HOVER       EventType = "HOVER"
	MOUSE_MOVE  EventType = "MOUSE_MOVE"
	MOUSE_HOVER EventType = "MOUSE_HOVER"
	DRAG        EventType = "DRAG"
	DROP        EventType = "DROP"
)

//a struct to rep web event details
type WebEventData struct {
	Source    string    `json:"source"`
	EventType EventType `json:"type"`
	Meta      EventType `json:"meta"`
	CreatedAt time.Time `json:"created_at"`
}
