package models

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"unknown-api/tools"
)

var db *gorm.DB //database

func init() {
	dbUri := tools.GetEnvironment().DatabaseUri
	logrus.Info("database server :", dbUri)
	conn, err := gorm.Open("postgres", dbUri)
	if err != nil {
		logrus.Panic(err)
	}
	logrus.Info("Postrgres ok ")
	db = conn
	//db.Debug().AutoMigrate(&Account{}, &Contact{}) //Database migration*/
}

//returns a handle to the DB object
func GetDB() *gorm.DB {
	return db
}
