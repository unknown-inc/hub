package models

import (
	"time"
	"encoding/json"
)

//a struct to rep user account
type PageView struct {
	PageId           uint            `json:"page_id"`
	ID               uint            `json:"id"`
	Metadata         json.RawMessage `json:"meta_data"`
	IsMobile         bool            `json:"is_mobile"`
	IsDesktop        bool            `json:"is_desktop"`
	IsTablet         bool            `json:"is_tablet"`
	Os               string          `json:"os"`

	Browser          string          `json:"browser"`
	WidthResolution  int             `json:"width_resolution"`
	HeightResolution int             `json:"height_resolution"`
	Query            int             `json:"query"`

	CreatedAt        time.Time       `json:"created_at"`
	UpdatedAt        time.Time       `json:"updated_at"`
	IsTest         bool      `json:"is_test"`
}

func (pageView *PageView) ValidatePageView() (map[string]interface{}, bool) {
	return nil, true

}
func (pageView *PageView) CreatePageView() (map[string]interface{}) {
	if resp, ok := pageView.ValidatePageView(); !ok {
		return resp
	}

	if pageView.UpdatedAt.IsZero() {
		pageView.UpdatedAt = time.Now()
	}

	GetDB().Create(pageView)

	/* if pageView.ID <= 0 {
		return u.Message(false, "Failed to create a view, connection error.")
	}
	response := u.Message(true, "Account has been created")
	response["event"] = pageView
	return response */
	return nil
}
