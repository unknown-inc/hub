package models

import "time"

//a struct to rep user account
type Page struct {
	ID             uint      `json:"id"`
	DomainId       uint      `json:"domain_id"`
	CreatedAt      time.Time `json:"created_at"`
	UpdatedAt      time.Time `json:"updated_at"`
	Url            string    `json:"url"`
	FullUrl        string    `json:"full_url"`
	Path           string    `json:"path"`
	Host           string    `json:"host"`
	Fragment       string    `json:"fragment"`
	Parameters     string    `json:"parameters"`
	PageViewsCount uint      `json:"page_views_count"`
	IsTest         bool      `json:"is_test"`
}

func (page *Page) ValidatePage() (map[string]interface{}, bool) {
	return nil, true

}

func (page *Page) CreatePage() (map[string]interface{}) {
	if resp, ok := page.ValidatePage(); !ok {
		return resp
	}
	if page.UpdatedAt.IsZero() {
		page.UpdatedAt = time.Now()
	}
	GetDB().Create(page)
	return nil
}

func (page *Page) UpdatePage() (map[string]interface{}) {
	GetDB().Model(&page).Update(page)
	return nil
}
