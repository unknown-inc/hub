package models

import (
	"time"
	"unknown-api/tools/http_helper"
)

//a struct to rep user account
type PageViewLocation struct {
	ID        uint      `json:"id"`
	PageId    uint      `json:"page_id"`
	CreateAt  time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Country   string    `json:"country_iso_2"`
	Country3  string    `json:"country_iso_3"`
	CityName  string    `json:"city_name"`
	UUID      string    `json:"uuid"`
}

func (pageViewLocation *PageViewLocation) ValidatePageViewLocation() (map[string]interface{}, bool) {
	return nil, true

}
func (pageViewLocation *PageViewLocation) CreatePageViewLocation() (map[string]interface{}) {
	if resp, ok := pageViewLocation.ValidatePageViewLocation(); !ok {
		return resp
	}

	if pageViewLocation.UpdatedAt.IsZero() {
		pageViewLocation.UpdatedAt = time.Now()
	}
	GetDB().Create(pageViewLocation)

	if pageViewLocation.ID <= 0 {
		return http_helper.Message(false, "Failed to create a view, connection error.")
	}
	return nil
}
