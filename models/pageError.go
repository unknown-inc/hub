package models

import (
	"time"
	"encoding/json"
)

//a struct to rep user account
type PageError struct {
	PageId    uint            `json:"page_id"`
	ID        uint            `json:"id"`
	Metadata  json.RawMessage `json:"metadata"`
	IsMobile  bool            `json:"is_mobile"`
	IsDesktop bool            `json:"is_desktop"`
	IsTablet  bool            `json:"is_tablet"`
	Os        string          `json:"os"`

	Browser string `json:"browser"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	IsTest         bool      `json:"is_test"`
}

func (pageError *PageError) CreatePageError() (map[string]interface{}) {
	if resp, ok := pageError.ValidatePageView(); !ok {
		return resp
	}

	if pageError.UpdatedAt.IsZero() {
		pageError.UpdatedAt = time.Now()
	}

	GetDB().Create(pageError)

	/* if pageView.ID <= 0 {
		return u.Message(false, "Failed to create a view, connection error.")
	}
	response := u.Message(true, "Account has been created")
	response["event"] = pageView
	return response */
	return nil
}
func (pageError *PageError) ValidatePageView() (map[string]interface{}, bool) {
	return nil, true

}
