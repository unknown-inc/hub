package models

//a struct to rep user account
type Domain struct {
	DomainName string `json:"domain_name"`
	UserId    uint `json:"user_id"`
}
