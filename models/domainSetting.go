package models

//a struct to rep user account
type DomainSetting struct {
	TrackGeo bool   `json:"track_geo"`
	Origins  string `json:"origins"`
	DomainId uint   `json:"domain_id"`
	ID       uint   `json:"id"`
}
