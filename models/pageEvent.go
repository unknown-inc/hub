package models

import (
	"time"
	"encoding/json"
)

//a struct to rep user account
type PageEvent struct {
	PageId uint `json:"page_id"`
	ID     uint `json:"id"`
	Metadata         json.RawMessage `json:"metadata"`
	IsMobile         bool            `json:"is_mobile"`
	IsDesktop        bool            `json:"is_desktop"`
	IsTablet         bool            `json:"is_tablet"`
	Os               string          `json:"os"`

	Browser          string          `json:"browser"`
	IsTest         bool      `json:"is_test"`

	// mobilePageData   MobileEventData `json:"mobile_data"`
	SequenceId int `json:"id"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
