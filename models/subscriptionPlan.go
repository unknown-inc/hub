package models

import "encoding/json"

//a struct to rep user account
type SubscriptionPlan struct {
	ID            uint            `json:"id"`
	ViewsQuota    int             `json:"views_count"`
	HeatMapsCount int             `json:"heatmaps_count"`
	MonthPrice    int             `json:"month_price"`
	YearPrice     int             `json:"year_price"`
	Details       json.RawMessage `json:"details"`
}
