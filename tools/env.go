package tools

import (
	"sync"
	"os"
)

var once sync.Once

type env struct {
	RedisUri            string
	DatabaseUri         string
	ScreenShotUri       string
	ScreenShotSecretKey string
	RedisChannelName    string
	BucketTokenKey      string
	Docker              bool
	Port                string
}

var singleton *env
/*
ARG UNK_ANA_SCREENSHOT_HOST
ARG UNK_ANA_DATABASE_URI
ARG UNK_ANA_TRACK_CHANNEL_NAME

var TrackChannelName = os.Getenv("TRACK_CHANNEL_NAME")
// track-web_development:web_notification_channel:dashboard_notification:
 */
func GetEnvironment() *env {
	screenshotUriEnValue := os.Getenv("UNK_ANA_SCREENSHOT_HOST")
	once.Do(func() {
		singleton = &env{
			RedisUri:            os.Getenv("UNK_ANA_REDIS_URI"),
			RedisChannelName:    os.Getenv("UNK_ANA_TRACK_CHANNEL_NAME"),
			DatabaseUri:         os.Getenv("UNK_ANA_DATABASE_URI"),
			Docker:              os.Getenv("DOCKER") == "1",
			Port:                os.Getenv("PORT"),
			BucketTokenKey:      os.Getenv("UNK_ANA_BUCKET_TOKEN_KEY"),
			ScreenShotUri:       screenshotUriEnValue + "/screen",
			ScreenShotSecretKey: os.Getenv("UNK_ANA_SCREENSHOT_SECRET_KEY"),
		}
	})
	return singleton
}
