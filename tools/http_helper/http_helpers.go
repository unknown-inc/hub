package http_helper

import (
	"encoding/json"
	"net/http"
	"github.com/mssola/user_agent"
	"io/ioutil"
	"bytes"
	"errors"
	"unknown-api/tools/interfaces"
)

var ua = user_agent.New("")

const ERROR_SERVER = "error server"

func ParseUserAgent(userAgentString string) *user_agent.UserAgent {
	ua.Parse(userAgentString)
	return ua
}

func Message(status bool, message string) (map[string]interface{}) {
	return map[string]interface{}{"status": status, "message": message}
}

func Respond(w http.ResponseWriter, data map[string]interface{}) {
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(data)
}

func ReadBody(r *http.Request) (map[string]interface{}, interface{}) {
	body := make(map[string]interface{})
	// Read the Body content
	var bodyBytes []byte
	if r.Body != nil {
		bodyBytes, _ = ioutil.ReadAll(r.Body)
	}
	err := json.Unmarshal(bodyBytes, &body)
	r.Body.Close()
	return body, err
}

func RestoreBody(r *http.Request, bodyBytes []byte) {
	// Restore the io.ReadCloser to its original state
	r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
}

func ReadBodyAsInterface(r *http.Request, object interface{}) interface{} {
	// Read the Body content
	var bodyBytes []byte
	if r.Body != nil {
		bodyBytes, _ = ioutil.ReadAll(r.Body)
	}
	err := json.Unmarshal(bodyBytes, &object)
	r.Body.Close()
	return err
}

func ReplyError(w http.ResponseWriter, status int, message string) {
	response := make(map[string]interface{})
	response = Message(false, message)
	w.WriteHeader(status)
	w.Header().Add("Content-Type", "application/json")
	Respond(w, response)
}

func ParsePostParams(r *http.Request) (interface{}, interfaces.RequestViewData) {
	var data interfaces.RequestViewData
	err := ReadBodyAsInterface(r, &data)
	if err != nil {
		return err, data
	}
	switch r.Method {
	case "POST":
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err != nil {
			return errors.New("ParseForm"), data

		}
		if (len(data.Uri)) < 4 {
			return errors.New("Empty Uri"), data
		}
		// restart body
		bytes, _ := json.Marshal(data)
		RestoreBody(r, bytes)

		return nil, data
	default:
		return errors.New("Only POST method are supported."), data
	}
}
