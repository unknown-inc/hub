package http_helper

const (
	CONTEXT_REQUEST_DOMAIN_ID string = "request_domain_id"
	CONTEXT_REQUEST_USER_ID   string = "request_user_id"
	CONTEXT_REQUEST_IS_TEST   string = "request_is_test"
	CONTEXT_REQUEST_PAGE_ID   string = "request_page_id"
)
