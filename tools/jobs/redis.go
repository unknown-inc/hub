package jobs

import (
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"unknown-api/tools"
)

var client *redis.Client

func init() {
	var redisAddr = tools.GetEnvironment().RedisUri
	logrus.Info("IS Running in docker env ?", tools.GetEnvironment().Docker)
	logrus.Info("@ redis ", redisAddr)

	client = redis.NewClient(&redis.Options{
		Addr:     redisAddr,
		Password: "", // no password set
		DB:       1,  // use default DB
	})
	_, err := client.Ping().Result()
	if err != nil {
		// FIXME do not panic
		logrus.Error("ERROR :: redis is down", err);
	} else {
		logrus.Info("Redis is Fine");
	}

}

//returns a handle to the DB object
func GetRedisClient() *redis.Client {
	return client
}
