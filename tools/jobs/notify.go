package jobs

import (
	"fmt"
	"unknown-api/tools"
	"github.com/sirupsen/logrus"
)

// Notify user via redis
func NotifyUserNewView(params ...string) {
	var channelName = tools.GetEnvironment().RedisChannelName
	var s string
	if len(params) > 1 {
		channelName = params [0]
		s = params[1]
	} else {
		s = params [0]
	}

	err := GetRedisClient().Publish(channelName, s).Err()
	if err != nil {
		logrus.Error(err)
	} else {
		fmt.Println("Sent")
	}
}
