package interfaces

// Post request data
type RequestViewData struct {
	Uri     string      `json:"uri,omitempty"`
	Browser BrowserInfo `json:"browser,omitempty"`
	Utm     UtmInfo     `json:"utm,omitempty"`
}

// Post request data
type RequestErrorData struct {
	Uri     string      `json:"uri,omitempty"`
	Browser BrowserInfo `json:"browser,omitempty"`
	Utm     UtmInfo     `json:"utm,omitempty"`
}

// Utm info from the uri
type UtmInfo struct {
	Name   string `json:"name"`
	Source string `json:"source"`
}

/*
// user agent
ua: navigator.userAgent,
// window width
ww: window.innerWidth,
// window height
wh: window.innerHeight
 */
type BrowserInfo struct {
	Ua string `json:"ua,omitempty"`
	Ww int    `json:"ww,omitempty"`
	Wh int    `json:"wh,omitempty"`
}

// Event that will be sent to user about the page id
type EventNewViewForPageMessage struct {
	PageId   uint `json:"page_id"`
	DomainId uint `json:"domain_id"`
}


// Event that will be sent to user about the page id
type EventNewErrorForPageMessage struct {
	PageId   uint `json:"page_id"`
	DomainId uint `json:"domain_id"`
}
