package geo_db

import (
	"github.com/oschwald/maxminddb-golang"
	"net"
	"github.com/sirupsen/logrus"
	"path"
	"runtime"
)

type GeoResult struct {
	Country struct {
		ISOCode string `maxminddb:"iso_code"`
	} `maxminddb:"country"`
	City struct {
		GeoNameID uint              `maxminddb:"geoname_id"`
		Names     map[string]string `maxminddb:"names"`
	} `maxminddb:"city"`
}

var geoReader *maxminddb.Reader //database

func init() {
	_, filename, _, _ := runtime.Caller(0)
	reader, err := maxminddb.Open(path.Join(path.Dir(filename), "GeoLite2-City_20190625", "GeoLite2-City.mmdb"))
	if err != nil {
		logrus.Error("ERROR :: opening DB", err)
	} else {
		logrus.Info("OK geo db")
	}
	geoReader = reader
}

//returns a handle to the DB object
func ParseIp(ipString string) (error, GeoResult) {
	var record GeoResult
	ip := net.ParseIP(ipString)
	err := geoReader.Lookup(ip, &record)
	return err, record
}
