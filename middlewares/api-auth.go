package middlewares

import (
	"net/http"
	"strings"
	"context"
	"unknown-api/models"
	"github.com/jinzhu/gorm"
	"encoding/json"
	"unknown-api/tools/http_helper"
)

var DOMAIN_BY_PUBLIC_KEY_WHERE_QUERY = "public_key = ?"

var ApiAuthentication = func(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenGrabbedFromHeader := true
		tokenValue := r.Header.Get("Authorization") //Grab the token from the header

		// if not in header look inside body , avoid cors
		if tokenValue == "" {
			tokenGrabbedFromHeader = false
			body, err := http_helper.ReadBody(r)
			if err != nil {
				http_helper.ReplyError(w, http.StatusBadRequest, "bad request")
			}
			tokenValue, err = body["token"].(string)
			if tokenValue == "" {
				http_helper.ReplyError(w, http.StatusForbidden, "Missing auth token")
				return
			}
			// restart body
			bytes, _ := json.Marshal(body)
			http_helper.RestoreBody(r, bytes)
		}

		if tokenValue == "" { //Token is missing, returns with error code 403 Unauthorized
			http_helper.ReplyError(w, http.StatusForbidden, "Missing auth token")
			return
		}
		// if we grab the token from header, then we need to split it and only take the first part
		if tokenGrabbedFromHeader {
			splitted := strings.Split(tokenValue, " ") //The token normally comes in format `Bearer {token-body}`, we check if the retrieved token matched this requirement
			if len(splitted) != 2 {
				http_helper.ReplyError(w, http.StatusForbidden, "Invalid/Malformed token")
				return
			}
			tokenValue = splitted[1] //Grab the token part, what we are truly interested in
		}

		// Token is ok
		var token models.ApiKey

		err := models.GetDB().Where(DOMAIN_BY_PUBLIC_KEY_WHERE_QUERY, tokenValue).Find(&token)
		if err.Error != nil {
			var message string
			var status int
			if gorm.IsRecordNotFoundError(err.Error) {
				message = "Invalid/Malformed auth token"
				status = http.StatusForbidden

			} else {
				message = http_helper.ERROR_SERVER
				status = http.StatusInternalServerError
			}
			http_helper.ReplyError(w, status, message)
			return
		}
		//Everything went well, proceed with the request and set the caller to the user retrieved from the parsed token
		ctx := context.WithValue(r.Context(), http_helper.CONTEXT_REQUEST_DOMAIN_ID, token.DomainId)
		ctx = context.WithValue(ctx, http_helper.CONTEXT_REQUEST_IS_TEST, token.IsTest)
		ctx = context.WithValue(ctx, http_helper.CONTEXT_REQUEST_USER_ID, token.UserId)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r) //proceed in the middleware chain!
	});
}
