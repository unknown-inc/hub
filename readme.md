Api hub for [unknownanalytics](unknownanalytics.com/).

In red square : 

![module](docs/hub.arch.png)

This is the base api module that collects web pages views, page events for unknownanalytics clients. 

Env variable to configure

```
UNK_ANA_REDIS_URI
UNK_ANA_TRACK_CHANNEL_NAME
UNK_ANA_DATABASE_URI
UNK_ANA_SCREENSHOT_HOST
UNK_ANA_BUCKET_TOKEN_KEY
```

## ENV 

- COMMON
```
UNK_ANA_TRACK_CHANNEL_NAME=unk_track-web_<your_env>:web_notifications:dashboard_notification
UNK_ANA_BUCKET_TOKEN_KEY=unk_bucket_token_prefix_<your_env>
```

- Environment LOCALE (& Docker Linux)
```
UNK_ANA_DATABASE_URI=postgres://<user>:<pass>@localhost/<db_name>
UNK_ANA_SCREENSHOT_HOST=http://localhost:3000
UNK_ANA_REDIS_URI=127.0.0.1
```

- DOCKER Environment  for windows
```
UNK_ANA_DATABASE_URI=postgres://<user>:<pass>@host.docker.internal/<db_name>?sslmode=disable
UNK_ANA_SCREENSHOT_HOST=http://host.docker.internal:3000
UNK_ANA_REDIS_URI=host.docker.internal:6379
```