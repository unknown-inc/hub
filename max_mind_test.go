package main_test

import (
	"log"
	"net"
	"fmt"
	"github.com/oschwald/maxminddb-golang"
	"testing"
	"github.com/stretchr/testify/require"
)

func TestMaxMind(t *testing.T) {
	db, err := maxminddb.Open("geo-db/GeoLite2-City_20190625/GeoLite2-City.mmdb")
	if err != nil {
		require.Nil(t, err, "unexpected error while opening database: %v", err)
		log.Fatal(err)
	}
	defer db.Close()

	ip := net.ParseIP("81.2.69.142")

	var record struct {
		Country struct {
			ISOCode string `maxminddb:"iso_code"`
		} `maxminddb:"country"`
		City struct {
			GeoNameID uint              `maxminddb:"geoname_id"`
			Names     map[string]string `maxminddb:"names"`
		} `maxminddb:"city"`
	} // Or any appropriate struct

	err = db.Lookup(ip, &record)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(record.Country.ISOCode)
	fmt.Println(record.City.GeoNameID)
	fmt.Println(record.City.Names["fr"])
}
