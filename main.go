package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"io"
	"unknown-api/api"
	"unknown-api/tools"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"unknown-api/middlewares"
	"github.com/sirupsen/logrus"
	"github.com/rs/cors"
	"unknown-api/api/api_v1"
)

func HealthCheckHandler(w http.ResponseWriter, r *http.Request) {
	// A very simple health check.
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	// In the future we could report back on the status of our DB, or our cache
	// (e.g. Redis) by performing a simple PING, and include them in the response.
	io.WriteString(w, `{"alive": true}`)

}

func main() {

	tools.InitLogger(true)
	logrus.Info("starting ...")
	router := mux.NewRouter()

	/* Public and metrics */

	public := router.PathPrefix("/public").Subrouter()

	// disable IPs
	public.HandleFunc("/health", HealthCheckHandler)
	public.Handle("/metrics", promhttp.Handler())

	/* Protected route */

	protected := router.PathPrefix("/").Subrouter();
	protected.Use(middlewares.ApiAuthentication) // Auth middlewre
	protected.Use(api.RateMiddleware)            //Rate Auth middlewre
	protected.Use(api.PlanMiddleware)            //Plan subscriptions middlewre

	/* Routes api/v1/ */

	hub1 := protected.PathPrefix("/hub/v1").Subrouter()
	// Intercept
	hub1.Use(api_v1.OriginMiddle)
	hub1.Use(api_v1.PageMiddleware)
	hub1.HandleFunc("/view", api_v1.View)
	hub1.HandleFunc("/event", api_v1.Event)
	//hub1.HandleFunc("/error", api_v1.Error)



	port := tools.GetEnvironment().Port //Get port from env var

	if port == "" {
		port = "8000" //localhost
	}

	logrus.Info("starting app on " + port)

	// Use default options
	handler := cors.Default().Handler(router)

	err := http.ListenAndServe(":"+port, handler) //Launch the middlewares, visit localhost:8000/api

	if err != nil {
		logrus.Panic(err)
	}
}
