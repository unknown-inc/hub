# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from the latest golang base image
FROM golang:latest

# Add Maintainer Info
LABEL maintainer="Fathallah Houssem <fathallah.houssem@gmail.com>"

ARG UNK_ANA_REDIS_URI
ARG UNK_ANA_SCREENSHOT_HOST
ARG UNK_ANA_DATABASE_URI
ARG UNK_ANA_TRACK_CHANNEL_NAME
# Tell app that we run on docker
ENV DOCKER=1



# Copy the source from the current directory to the Working Directory inside the container
COPY . /go/src/unknown-api

# Set the Current Working Directory inside the container
WORKDIR /go/src/unknown-api

# Get modules
RUN go get -v -t .

#

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build  -o /app

CMD ["/app"]

# Expose port 8080 to the outside world
EXPOSE 8000
