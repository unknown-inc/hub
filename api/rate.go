package api

import (
	"net/http"
	"strconv"
	"time"
	"fmt"
	"github.com/sirupsen/logrus"

	"unknown-api/tools/jobs"
	"unknown-api/tools/http_helper"
	"unknown-api/tools"
)

var keySuffix = tools.GetEnvironment().BucketTokenKey

var bucketTokenNumber int64 = 20
var bucketExpiration time.Duration = 5

func buildKeyString(u uint) string {
	return keySuffix + strconv.FormatUint(uint64(u), 10)
}

// Check if a key exists in redis
func checkKeyExists(identifier uint) bool {
	println(buildKeyString(identifier))
	check := jobs.GetRedisClient().Exists(buildKeyString(identifier))
	return check.Val() == 1
}

// Update redis rate limit for current domain
func UpdateRateViewLimitForCurrentDomain(identifier uint) {
	jobs.GetRedisClient().Decr(buildKeyString(identifier))
}

var RateMiddleware = func(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Everything went well, proceed with the request and set the caller to the user retrieved from the parsed token
		domainString := r.Context().Value(http_helper.CONTEXT_REQUEST_DOMAIN_ID) //Grab the id of the domain that send the request
		if domainString == "" {
			http_helper.ReplyError(w, http.StatusNotFound, "Domain not found ")
			return
		}
		domainId := domainString.(uint)
		key := buildKeyString(domainId)
		if checkKeyExists(domainId) {
			remainsTokenString := jobs.GetRedisClient().Get(key).Val()

			fmt.Println(remainsTokenString)
			remains, _ := strconv.ParseInt(remainsTokenString, 10, 64)
			logrus.Info("Request :", r.RemoteAddr, " # Remains : ", remains)

			if remains < 0 {
				response := http_helper.Message(false, "Too many request")
				w.WriteHeader(http.StatusTooManyRequests)
				http_helper.Respond(w, response)
				return
			}
		} else {
			jobs.GetRedisClient().Set(key, strconv.FormatInt(bucketTokenNumber, 10), bucketExpiration*time.Second)
		}
		UpdateRateViewLimitForCurrentDomain(domainId)
		next.ServeHTTP(w, r) //proceed in the middleware chain!
	});
}
