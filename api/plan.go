package api

import (
	"net/http"
	"unknown-api/tools/http_helper"
)

func updateUserPlanValues(domainId uint) {

}

var PlanMiddleware = func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Everything went well, proceed with the request and set the caller to the user retrieved from the parsed token
		domainString := r.Context().Value(http_helper.CONTEXT_REQUEST_DOMAIN_ID) //Grab the id of the domain that send the request
		if domainString == "" {
			http_helper.ReplyError(w, http.StatusNotFound, "Domain not found ")
			return
		}
		domainId := domainString.(uint)
		//
		updateUserPlanValues(domainId)
		next.ServeHTTP(w, r) //proceed in the middleware chain!
	});
}
