package api

import (
	"testing"
	"fmt"
	"net/http"
	"unknown-api/tools"
	"bytes"
	"github.com/sirupsen/logrus"
)

// getDomainAndDomainSettings retrieve a domain setting for a given domain
func TestGetDomainAndDomainSettings(t *testing.T) {

}

//  v Create page for a given uri string
func TestCreatePage(t *testing.T) {

}

// createPageView Creates a new view for a given page id.
func TestCreatePageView(t *testing.T) {

}

// getDomainAndDomainSettings retrieve a domain setting for a given domain
func TestCreatePageError(t *testing.T) {

}

// When this features is active, we create
func TestCreatePageScreenTest(t *testing.T) {
	payload := `{"page_id":55,"url":"http://host.docker.internal:3002", "page_url" : "http://host.docker.internal:3002", "app_token" : "` + tools.GetEnvironment().ScreenShotSecretKey + `" }`
	payloadJson := []byte(payload)
	resp, err := http.Post(tools.GetEnvironment().ScreenShotUri, "application/json", bytes.NewBuffer(payloadJson))
	if err != nil {
		logrus.Error("screen shooter response is Not OK ")
		logrus.Error(err)
	}
	if resp != nil {
		// Print the HTTP Status Code and Status Name
		fmt.Println("HTTP Response Status:", resp.StatusCode, http.StatusText(resp.StatusCode))

		if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
			fmt.Println("HTTP Status is in the 2xx range")
		} else {
			fmt.Println("Argh! Broken")
		}
	}
}

// When geo track is enabled, we create a geo lookup from request ip@address
func TestCreatePageViewGeoIPLocation(t *testing.T) {

}
