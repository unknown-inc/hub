package api_v1

import (
	"net/http"
	"unknown-api/tools/http_helper"
	"unknown-api/tools/interfaces"
	"encoding/json"
	"unknown-api/tools/jobs"
	"github.com/sirupsen/logrus"
	"strconv"
	"unknown-api/tools"
	"unknown-api/api"
)

var View = func(w http.ResponseWriter, r *http.Request) {

	response := make(map[string]interface{})

	domainId, okForDomain := r.Context().Value(http_helper.CONTEXT_REQUEST_DOMAIN_ID).(uint) //Grab the id of the domain that send the request
	// sandbox
	isTest, _ := r.Context().Value(http_helper.CONTEXT_REQUEST_IS_TEST).(bool)         // Grab is sandbox
	pageId, okForPage := r.Context().Value(http_helper.CONTEXT_REQUEST_PAGE_ID).(uint) //Grab the id of the page that send the request
	if !okForDomain || !okForPage {
		response = http_helper.Message(false, http_helper.ERROR_SERVER)
		w.WriteHeader(http.StatusBadRequest)
		http_helper.Respond(w, response)
		return
	}
	// get prams data again

	errParsing, data := http_helper.ParsePostParams(r)

	if errParsing != nil {
		response = http_helper.Message(false, http_helper.ERROR_SERVER)
		w.WriteHeader(http.StatusBadRequest)
		http_helper.Respond(w, response)
		return

	}
	//
	// create page view
	err := api.CreatePageView(pageId, data, isTest, r)
	if err != nil {
		response = http_helper.Message(false, http_helper.ERROR_SERVER)
		w.WriteHeader(http.StatusInternalServerError)
		http_helper.Respond(w, response)
		return

	}
	status, errorString, domainSetting, _ := api.GetDomainAndDomainSettings(domainId)

	// if not found or something went bad
	if status != 0 {
		response = http_helper.Message(false, errorString)
		w.WriteHeader(status)
		http_helper.Respond(w, response)
		return
	}

	// if track geo is enabled
	if domainSetting.TrackGeo {
		go api.CreatePageViewGeoIPLocation(pageId, r)
	}

	// create scren shot

	go api.CreatePageScreen(pageId, data.Uri)

	// notify user if it's connected
	event := &interfaces.EventNewViewForPageMessage{
		PageId:   pageId,
		DomainId: domainId,
	}

	eventMarshaled, _ := json.Marshal(event)
	userId := r.Context().Value(http_helper.CONTEXT_REQUEST_USER_ID).(uint) //Grab the id of the domain that send the request
	userIdAsString := strconv.FormatUint(uint64(userId), 10)
	var channelName = tools.GetEnvironment().RedisChannelName

	jobs.NotifyUserNewView(channelName+":"+userIdAsString, string(eventMarshaled))

	logrus.Info("notification " + channelName + ":" + userIdAsString)

	http_helper.Respond(w, response)
}
