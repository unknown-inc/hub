package api_v1

import (
	"net/http"
	"strings"
	"context"
	"unknown-api/tools/http_helper"
	"unknown-api/api"
)

var OriginMiddle = func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := make(map[string]interface{})
		domainId, ok := r.Context().Value(http_helper.CONTEXT_REQUEST_DOMAIN_ID).(uint) //Grab the id of the domain that send the request
		if !ok {
			response = http_helper.Message(false, http_helper.ERROR_SERVER)
			http.Error(w, "Origin not allowed", http.StatusBadRequest)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		status, errorString, domainSetting, _ := api.GetDomainAndDomainSettings(domainId)

		// if not found or something went bad
		if status != 0 {
			response = http_helper.Message(false, errorString)
			w.WriteHeader(status)
			http_helper.Respond(w, response)
			return
		}

		// verify origin

		allowedOrigins := strings.Split(domainSetting.Origins, ",")
		origin := r.Header.Get("Origin")
		// Verify that request has an origin handler
		if origin == "" {
			response = http_helper.Message(false, http_helper.ERROR_SERVER)
			http.Error(w, "Origin not allowed", http.StatusBadRequest)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		allowedOrigin := false
		for _, s := range allowedOrigins {
			if s == origin {
				allowedOrigin = true
				break
			}
		}

		if !allowedOrigin {
			response = http_helper.Message(false, http_helper.ERROR_SERVER)
			http.Error(w, "Origin not allowed", http.StatusBadRequest)
			w.WriteHeader(http.StatusBadRequest)
			http_helper.Respond(w, response)
			return
		}
		next.ServeHTTP(w, r) //proceed in the middleware chain!
	});
}

var PageMiddleware = func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		response := make(map[string]interface{})
		domainId, ok := r.Context().Value(http_helper.CONTEXT_REQUEST_DOMAIN_ID).(uint)
		isTest, _ := r.Context().Value(http_helper.CONTEXT_REQUEST_IS_TEST).(bool)
		if !ok {
			response = http_helper.Message(false, http_helper.ERROR_SERVER)
			w.WriteHeader(http.StatusBadRequest)
			http_helper.Respond(w, response)
			return
		}

		errParsing, data := http_helper.ParsePostParams(r)

		if errParsing != nil {
			response = http_helper.Message(false, http_helper.ERROR_SERVER)
			w.WriteHeader(http.StatusBadRequest)
			http_helper.Respond(w, response)
			return

		}
		err, pageId := api.CreateIfNotExistPage(domainId, data.Uri, isTest)
		// something went bad
		if err.Error != nil {
			response = http_helper.Message(false, http_helper.ERROR_SERVER)
			w.WriteHeader(http.StatusInternalServerError)
			http_helper.Respond(w, response)
			return
		}
		//Everything went well, proceed with the request and set the page_id
		ctx := context.WithValue(r.Context(), http_helper.CONTEXT_REQUEST_PAGE_ID, pageId)
		r = r.WithContext(ctx)
		next.ServeHTTP(w, r) //proceed in the middleware chain!
	});
}
