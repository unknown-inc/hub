package api_v1
/*
import (
	"net/http"
	"unknown-api/tools/http_helper"
	"unknown-api/tools/interfaces"
	"encoding/json"
	"strconv"
	"unknown-api/tools/jobs"
	"github.com/sirupsen/logrus"
	"unknown-api/tools"
	"unknown-api/api"
)

var Error = func(w http.ResponseWriter, r *http.Request) {
	response := make(map[string]interface{})
	pageId := r.Context().Value(http_helper.CONTEXT_REQUEST_PAGE_ID).(uint) //Grab the id of the page that send the request
	// get prams data again

	errParsing, data := http_helper.ParsePostParams(r)

	if errParsing != nil {
		response = http_helper.Message(false, http_helper.ERROR_SERVER)
		w.WriteHeader(http.StatusBadRequest)
		http_helper.Respond(w, response)
		return

	}
	//
	// create page view
	err := api.CreatePageView(pageId, data, r)
	if err != nil {
		response = http_helper.Message(false, http_helper.ERROR_SERVER)
		w.WriteHeader(http.StatusInternalServerError)
		http_helper.Respond(w, response)
		return
	}

	domainId := r.Context().Value(http_helper.CONTEXT_REQUEST_DOMAIN_ID).(uint) //Grab the id of the domain

	status, errorString, _, _ := api.GetDomainAndDomainSettings(domainId)

	// if not found or something went bad
	if status != 0 {
		response = http_helper.Message(false, errorString)
		w.WriteHeader(status)
		http_helper.Respond(w, response)
		return
	}

	// notify user if it's connected
	event := &interfaces.EventNewErrorForPageMessage{
		PageId:   pageId,
		DomainId: domainId,
	}

	eventMarshaled, _ := json.Marshal(event)
	userId := r.Context().Value(http_helper.CONTEXT_REQUEST_USER_ID).(uint) //Grab the id of the domain that send the request
	userIdAsString := strconv.FormatUint(uint64(userId), 10)

	var channelName = tools.GetEnvironment().RedisChannelName
	jobs.NotifyUserNewView(channelName+":"+userIdAsString, string(eventMarshaled))

	logrus.Info("notification " + channelName + ":" + userIdAsString)

	http_helper.Respond(w, response)
}
*/
